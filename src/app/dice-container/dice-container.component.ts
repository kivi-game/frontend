import { Component, OnInit } from '@angular/core';
import { faDiceOne, faDiceTwo, faDiceThree, faDiceFour, faDiceFive, faDiceSix } from '@fortawesome/free-solid-svg-icons'
import { DataService } from '../data.service';

@Component({
  selector: 'app-dice-container',
  templateUrl: './dice-container.component.html',
  styleUrls: ['./dice-container.component.css']
})
export class DiceContainerComponent implements OnInit {
  faDice = [faDiceOne, faDiceTwo, faDiceThree, faDiceFour, faDiceFive, faDiceSix];
  
  dice = [];
  combinations = [];

  constructor(private dataService: DataService) { }

  ngOnInit(): void {
    this.roll();
  }

  roll(): void {
    this.dataService.sendGetRequest().subscribe((data: any) => {
      this.dice = data.dice;
      this.combinations = data.combinations;
    });
  }

}
